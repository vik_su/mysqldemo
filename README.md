# Tutorial Dasar Interaksi MySQL PHP Server - Android menggunakan JSON #

Tutorial ini adalah tutorial dasar yang membahas interaksi Aplikasi Android dengan PHP MySQL Server via JSON.

### Apa saja yang ada di tutorial ini ###

* Insert data (pada fitur register).
* Select 1 data (pada fitur login).
* Passing hasil AsyncBackgroundTask ke Activity pemanggilnya (pada fitur login)
* Select >1 data dan parsing hasilnya ke dalam listView (HomeActivity / halaman setelah login)