package id.go.bps.perbendaharaan.mysqldemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import id.go.bps.perbendaharaan.mysqldemo.adapter.UserInfoAdapter;
import id.go.bps.perbendaharaan.mysqldemo.bgTask.UserInfoBgTask;
import id.go.bps.perbendaharaan.mysqldemo.model.UserInfo;
import id.go.bps.perbendaharaan.mysqldemo.response.AsyncResponse;

public class HomeActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

    }
}
