package id.go.bps.perbendaharaan.mysqldemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import id.go.bps.perbendaharaan.mysqldemo.bgTask.UserInfoBgTask;

public class RegisterActivity extends AppCompatActivity {
    EditText et_name,et_username,et_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        et_name = (EditText) findViewById(R.id.editName);
        et_username = (EditText) findViewById(R.id.editUsername);
        et_password = (EditText) findViewById(R.id.editPassword);

    }

    public void register(View view){

        String method = "register";
        UserInfoBgTask bgTask = new UserInfoBgTask(this);
        bgTask.execute(method,et_name.getText().toString(),et_username.getText().toString(),et_password.getText().toString());
        finish();

    }

}
