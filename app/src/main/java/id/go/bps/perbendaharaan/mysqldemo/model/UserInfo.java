package id.go.bps.perbendaharaan.mysqldemo.model;

/**
 * Created by Viktor on 29/06/2016.
 */
public class UserInfo {
    private String name,user_name,user_pass;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_pass() {
        return user_pass;
    }

    public void setUser_pass(String user_pass) {
        this.user_pass = user_pass;
    }
}
