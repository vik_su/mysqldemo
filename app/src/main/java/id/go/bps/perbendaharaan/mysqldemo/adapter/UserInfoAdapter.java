package id.go.bps.perbendaharaan.mysqldemo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.go.bps.perbendaharaan.mysqldemo.R;
import id.go.bps.perbendaharaan.mysqldemo.model.UserInfo;

/**
 * Created by viktor.suwiyanto on 30/06/2016.
 */
public class UserInfoAdapter extends ArrayAdapter {

    List list = new ArrayList();

    public UserInfoAdapter(Context context, int resource) {
        super(context, resource);
    }

    public void add(UserInfo object) {
        super.add(object);
        list.add(object);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
        row = convertView;
        UserInfoHolder userInfoHolder;
        if(row == null){
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.row_layout,parent,false);
            userInfoHolder = new UserInfoHolder();
            userInfoHolder.textName = (TextView) row.findViewById(R.id.textName);
            userInfoHolder.textUsername = (TextView) row.findViewById(R.id.textUsername);
            userInfoHolder.textPassword = (TextView) row.findViewById(R.id.textPassword);
            row.setTag(userInfoHolder);
        }else{
            userInfoHolder = (UserInfoHolder) row.getTag();
        }
        UserInfo userInfo = (UserInfo) this.getItem(position);
        userInfoHolder.textName.setText(userInfo.getName());
        userInfoHolder.textUsername.setText(userInfo.getUser_name());
        userInfoHolder.textPassword.setText(userInfo.getUser_pass());
        return row;
    }

    static class UserInfoHolder{
        TextView textName,textUsername,textPassword;
    }

}
