package id.go.bps.perbendaharaan.mysqldemo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import id.go.bps.perbendaharaan.mysqldemo.bgTask.UserInfoBgTask;
import id.go.bps.perbendaharaan.mysqldemo.model.UserInfo;
import id.go.bps.perbendaharaan.mysqldemo.response.AsyncResponse;

public class MainActivity extends AppCompatActivity implements AsyncResponse {
    EditText et_username,et_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent intent = new Intent(this,SplashActivity.class);
        startActivity(intent);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        et_username = (EditText) findViewById(R.id.editUsername);
        et_password = (EditText) findViewById(R.id.editPassword);
    }

    public void regUser(View view){
        startActivity(new Intent(this,RegisterActivity.class));
    }

    public void loginUser(View view){

        String method = "login";
        UserInfoBgTask bgTask = new UserInfoBgTask(this);
        bgTask.delegate = this;
        bgTask.execute(method,et_username.getText().toString(),et_password.getText().toString());

    }

    @Override
    public void processFinish(UserInfo userInfo) {
        Intent intent = new Intent(this,HomeActivity.class);
        intent.putExtra("name",userInfo.getName());
        intent.putExtra("user_name",userInfo.getUser_name());
        startActivity(intent);
    }

    @Override
    public void sentBackStringData(String js_string) {

    }
}
