package id.go.bps.perbendaharaan.mysqldemo.response;

import id.go.bps.perbendaharaan.mysqldemo.model.UserInfo;

/**
 * Created by Viktor on 29/06/2016.
 */
public interface AsyncResponse {
    void processFinish(UserInfo userInfo);
    void sentBackStringData(String js_string);
}
