package id.go.bps.perbendaharaan.mysqldemo.bgTask;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import id.go.bps.perbendaharaan.mysqldemo.response.AsyncResponse;
import id.go.bps.perbendaharaan.mysqldemo.model.UserInfo;

/**
 * Created by Viktor on 29/06/2016.
 */
public class UserInfoBgTask extends AsyncTask<String,Void,String> {

    Context context;
    public AsyncResponse delegate = null;


    public UserInfoBgTask(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        String reg_url = "http://viksu.esy.es/andro_tutor/register.php";
        String login_url = "http://viksu.esy.es/andro_tutor/login.php";
        String list_user_url = "http://viksu.esy.es/andro_tutor/list_user.php";
        String encType = "UTF-8";
        String encType2 = "iso-8859-1";
        String method = params[0];
        if(method.equals("register")){
            UserInfo userInfo = new UserInfo();
            userInfo.setName(params[1]);
            userInfo.setUser_name(params[2]);
            userInfo.setUser_pass(params[3]);
            try {
                URL url = new URL(reg_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream os = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(os,encType));
                String data = URLEncoder.encode("name",encType) + "=" + URLEncoder.encode(userInfo.getName(),encType)+"&"
                        + URLEncoder.encode("user_name",encType) + "=" + URLEncoder.encode(userInfo.getUser_name(),encType)+"&"
                        + URLEncoder.encode("user_pass",encType) + "=" + URLEncoder.encode(userInfo.getUser_pass(),encType);
                bufferedWriter.write(data);
                bufferedWriter.flush();
                bufferedWriter.close();
                os.close();

                InputStream is = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is,encType2));
                String response = "";
                String line = "";
                while ((line = bufferedReader.readLine())!=null){
                    response += line;
                }
                bufferedReader.close();
                is.close();
                httpURLConnection.disconnect();
                return response;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else if(method.equals("login")){
            UserInfo userInfo = new UserInfo();
            userInfo.setUser_name(params[1]);
            userInfo.setUser_pass(params[2]);
            try {
                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream os = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(os,encType));
                String data = URLEncoder.encode("user_name",encType) + "=" + URLEncoder.encode(userInfo.getUser_name(),encType)+"&"
                        + URLEncoder.encode("user_pass",encType) + "=" + URLEncoder.encode(userInfo.getUser_pass(),encType);
                bufferedWriter.write(data);
                bufferedWriter.flush();
                bufferedWriter.close();
                os.close();

                InputStream is = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is,encType2));
                String response = "";
                String line = "";
                while ((line = bufferedReader.readLine())!=null){
                    response += line;
                }
                bufferedReader.close();
                is.close();
                httpURLConnection.disconnect();
                return response;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else if(method.equals("list_user")){
            try {
                URL url = new URL(list_user_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                //httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                //OutputStream os = httpURLConnection.getOutputStream();
                //BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(os,encType));
                //String data = URLEncoder.encode("user_name",encType) + "=" + URLEncoder.encode(userInfo.getUser_name(),encType)+"&"
                //        + URLEncoder.encode("user_pass",encType) + "=" + URLEncoder.encode(userInfo.getUser_pass(),encType);
                //bufferedWriter.write(data);
                //bufferedWriter.flush();
                //bufferedWriter.close();
                //os.close();

                InputStream is = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is,encType2));
                String response = "";
                String line = "";
                while ((line = bufferedReader.readLine())!=null){
                    response += line;
                }
                bufferedReader.close();
                is.close();
                httpURLConnection.disconnect();
                return response;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String result) {
        //super.onPostExecute(result);
        try {
            JSONObject jsonObject = new JSONObject(result);
            String method = jsonObject.getString("method");
            boolean success = jsonObject.getBoolean("success");
            if(method.equals("register") && success){
                Toast.makeText(context,"Registrasion Success...",Toast.LENGTH_LONG).show();
            }else if(method.equals("login") && success){
                UserInfo userInfo = new UserInfo();
                userInfo.setName(jsonObject.getString("name"));
                userInfo.setUser_name(jsonObject.getString("user_name"));
                delegate.processFinish(userInfo);
            }else if(method.equals("list_user") && success){
                String data = jsonObject.getString("data");
                //Toast.makeText(context,data,Toast.LENGTH_LONG).show();
                delegate.sentBackStringData(result);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
