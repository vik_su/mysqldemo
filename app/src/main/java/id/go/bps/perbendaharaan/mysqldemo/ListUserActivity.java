package id.go.bps.perbendaharaan.mysqldemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import id.go.bps.perbendaharaan.mysqldemo.adapter.UserInfoAdapter;
import id.go.bps.perbendaharaan.mysqldemo.bgTask.UserInfoBgTask;
import id.go.bps.perbendaharaan.mysqldemo.model.UserInfo;
import id.go.bps.perbendaharaan.mysqldemo.response.AsyncResponse;

public class ListUserActivity extends AppCompatActivity implements AsyncResponse {

    TextView tv_welcome;
    JSONObject jsonObject;
    JSONArray jsonArray;
    UserInfoAdapter userInfoAdapter;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_user);
        listView = (ListView) findViewById(R.id.listView);
        String name = getIntent().getExtras().getString("name");
        String user_name = getIntent().getExtras().getString("user_name");
        tv_welcome = (TextView) findViewById(R.id.tv_welcome);
        tv_welcome.setText("Welcome back, "+name);
        String method = "list_user";
        UserInfoBgTask bgTask = new UserInfoBgTask(this);
        bgTask.delegate = this;
        bgTask.execute(method);
    }

    @Override
    public void processFinish(UserInfo userInfo) {

    }

    @Override
    public void sentBackStringData(String js_string) {
        userInfoAdapter = new UserInfoAdapter(this,R.layout.row_layout);
        listView.setAdapter(userInfoAdapter);
        try {
            jsonObject = new JSONObject(js_string);
            jsonArray = jsonObject.getJSONArray("data");

            int count = 0;
            while (count<jsonArray.length()){
                JSONObject jso = jsonArray.getJSONObject(count);
                UserInfo userInfo = new UserInfo();
                userInfo.setName(jso.getString("name"));
                userInfo.setUser_name(jso.getString("user_name"));
                userInfo.setUser_pass(jso.getString("user_pass"));
                userInfoAdapter.add(userInfo);
                count++;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
